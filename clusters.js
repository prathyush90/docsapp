var cluster = require('cluster');

var numCPUs = require('os').cpus().length;




if (cluster.isMaster) {  
    for (var i = 0; i < numCPUs; i++) {
        // Create a worker
        cluster.fork();
    }
} else {
    // Workers share the TCP connection in this server
    var server = require('./server.js');
    cluster.on('exit', function(worker, code, signal) {  
	    console.log('Worker %d died with code/signal %s. Restarting worker...', worker.process.pid, signal || code);
	    cluster.fork();
	});
}