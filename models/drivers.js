var mongoose = require('mongoose');

/*
   This schema is for  drivers.The parameters are  id,x coordinate,y coordinates,current status
*/

var driversSchema = new mongoose.Schema({
   
   
   
   driver_id:{
      type:String,
      default:''
   },
   last_request_accepted_time:{
      type:Number,
      default:0
   },
   geo: {
    type: [Number],
    index: '2d'
  },
   // 0 for available and 1 for ontrip or busy
   driver_status:{
      type:Number,
      default:0
   }
   
  
   
   
   
   
});


 


var Drivers = mongoose.model('drivers',driversSchema);

Drivers.findById         = function(id){

   var p          =        new Promise(function(resolve,reject){

         var query   = Drivers.findOne({driver_id:id});
         query.exec()
         .then(data=>{
            resolve(data);
         })
         .catch(err=>{
            reject(err);
         })

   });

   return p;
}

Drivers.findAllDrivers   = function(){
   var p          =        new Promise(function(resolve,reject){

      var query    = Drivers.find().lean().distinct('driver_id');
      query.exec()
      .then(data=>{
         resolve(data);
      })
      .catch(err=>{
         reject(err);
      })

   });
   return p;
}







Drivers.findDrivers   = function(x,y){
   var p          =        new Promise(function(resolve,reject){

      var query    = Drivers.find({'geo': {
         $near: [
            x,
            y
         ],
         $maxDistance: 25

         }}).limit(3);
      query.exec()
      .then(data=>{
         var result = []
         data.map(function(res){
            result.push(res.driver_id);
         });
         resolve(result);
      })
      .catch(err=>{
         reject(err);
      })

   });
   return p;
}


Drivers.updateStatusCompleted  =  function(time){
   var p          =        new Promise(function(resolve,reject){
         Drivers.updateMany({driver_status:1,last_request_accepted_time:{$lte:time}},{$set:{driver_status:0}},function(err,data){
            if(err){
               reject(err);
            }else{
               
               resolve(data);
            }
         });
   });
}

Drivers.update   = function(id,status){
   var p          =        new Promise(function(resolve,reject){

      var date = new Date();
      var time = date.getTime();
      var query    = Drivers.findOneAndUpdate({driver_id:id},{driver_status:status,last_request_accepted_time:time},{upsert:false,new:true});
      query.exec()
      .then(data=>{
         resolve(data);
      })
      .catch(err=>{
         reject(err);
      })

   });

   return p;
};   


module.exports=Drivers;  

