var mongoose = require('mongoose');

/*
   This schema is for  request events.The parameters are  id,status,event start time,pick up time, and drivers list who have access to this request
*/

var requestSchema = new mongoose.Schema({
   
   
   
   driver_id:{
      type:String,
      default:''
   },
   request_start_time:{
      type:Number,
      default:0
   },
   request_accepted_time:{
      type:Number,
      default:0
   },
   user_id:{
      type:String,
      default:''
   },
   todrivers:[],
   // 0 for waiting and 1 for ontrip 2 for processed
   request_status:{
      type:Number,
      default:0
   }
   
  
   
   
   
   
});


 


var Requests = mongoose.model('requests',requestSchema);


Requests.findCount    =  function(){
   var p              =    new Promise(function(resolve,reject){
      Requests.find({request_status:0}).count(function(err, count){
         if(err) {
            reject(err)
         }else{
            resolve(count);
         }
      });
   });
   return p;   
}


Requests.forDriver    =    function(driver_id,status){

   var p              =    new Promise(function(resolve,reject){

         var query    = Requests.find({todrivers:{$all : [driver_id]},request_status:status}).sort({request_start_time:-1});
         query.exec()
         .then(data=>{
            resolve(data);
         })
         .catch(err=>{
            reject(err);
         });

   });

   return p;

};



Requests.forDriverWithStatus    =    function(driver_id,status){

   var p              =    new Promise(function(resolve,reject){

         var query    = Requests.find({driver_id:driver_id,request_status:status});
         query.exec()
         .then(data=>{
            resolve(data);
         })
         .catch(err=>{
            reject(err);
         });

   });

   return p;

};

Requests.findById      = function(id){
   var p              =    new Promise(function(resolve,reject){

         var query    = Requests.findOne({_id:id});
         query.exec()
         .then(data=>{
            resolve(data);
         })
         .catch(err=>{
            reject(err);
         });

   });

   return p;
}


Requests.update      = function(id,driver_id,status){
   var p              =    new Promise(function(resolve,reject){
         var date = new Date();
         var time = date.getTime();
         var query    = Requests.findOneAndUpdate({_id:id},{driver_id:driver_id,request_status:status,request_accepted_time:time},{upsert:false,new:true});
         query.exec()
         .then(data=>{
            resolve(data);
         })
         .catch(err=>{
            reject(err);
         });

   });

   return p;
}

Requests.updateStatusCompleted   = function(time){
   var p              =    new Promise(function(resolve,reject){
         Requests.updateMany({request_status:1,request_accepted_time:{$lte:time}},{ $set:{request_status:2}},function(err,data){
            if(err){
               reject(err);
            }else{
               
               resolve(data);
            }
         });
   });
   return p;
}

Requests.findAll              = function(){
   var p              =    new Promise(function(resolve,reject){
         var query    =    Requests.find().sort({request_start_time:-1})
         query.exec()
         .then(data=>{
            resolve(data);
         })
         .catch(err=>{
            reject(err);
         })

   });
   return p;
}



module.exports=Requests;  

