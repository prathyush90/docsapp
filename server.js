'use strict';

const bodyParser = require('body-parser'),
  express = require('express')
  

var path = require('path');

var app = express();

var config = require('config');

var fs    = require('fs');


var mongoose = require('mongoose');


var adminRoute  = require('./routes/admin.js');

var driversRoute  = require('./routes/drivers.js');

  

mongoose.connect(config.db,function(err){

	if(err) console.log(err);

});



mongoose.set('debug',true);//debug(true);







//http server
var http       = require('http').Server(app);
let io = require('socket.io')(http);
var socket = require('./socket/socket.js')(io);

var requestRoute  = require('./routes/request.js')(socket);
  
app.set('port', process.env.PORT || 3500);
global.appRoot = path.resolve(__dirname)+"/";
app.set('view engine', 'ejs');
app.use(express.static(path.join(__dirname, './client/public')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/adminapi'  ,adminRoute);
app.use('/requestapi',requestRoute);
app.use('/driverapi',driversRoute);
//incomeRoute


app.get('*',function(req,res,next){
	res.status(200).sendFile(path.join(__dirname,'./client/public/index.html'));
});


var requestSchema  =  require('./models/requests.js');
var driverSchema  =  require('./models/drivers.js');
function checkForStatusChanges(){
	var time = new Date().getTime();
	var threshold = time - 5 * 60 * 1000;
	requestSchema.updateStatusCompleted(threshold);
	driverSchema.updateStatusCompleted(threshold);
}

setInterval(function () { 
   checkForStatusChanges(); 
}, 30000); 



app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});



//https.setSecure(credentials);
http.listen(app.get('port'), function() {
  console.log('app is running on port : ', app.get('port'));
});


/* uncomment after getting certs

https.listen('6453',function(){
  console.log('https app is running on port : ', '6453');
});

*/
