import { DocsappPage } from './app.po';

describe('docsapp App', () => {
  let page: DocsappPage;

  beforeEach(() => {
    page = new DocsappPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
