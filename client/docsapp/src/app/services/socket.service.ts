import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import * as io from 'socket.io-client';
@Injectable()
export class SocketService {
  private socket;
  constructor() { }


  sendEvent(event,message){
  	this.socket.emit(event, message);
  }
  
  getMessages() {
    let observable = new Observable(observer => {
      this.socket = io('http://34.208.178.67:3500/',{transports: ['websocket']});
      this.socket.on('statuschange', (data:any) => {
        console.log("xxxx");
        observer.next(data);    
      });
      return () => {
        this.socket.disconnect();
      };  
    })     
    return observable;
  }  

}
