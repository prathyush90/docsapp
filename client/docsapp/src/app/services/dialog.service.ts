import { Injectable , ViewEncapsulation } from '@angular/core';
import { NgbModal , NgbModalRef ,ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { DialogComponent } from '../components/dialog/dialog.component'


@Injectable()
export class DialogService {

  constructor(private modalService: NgbModal) { }

  openDialog(message){
  		console.log(message)
	    const modalRef = this.modalService.open(DialogComponent);
	  	modalRef.componentInstance.message = message;
	  	modalRef.result.then(data=>{
	  		console.log(data);
	  	})
	  	.catch(err=>{
	  		console.log(err);
	  	});
	  	
  	
  }


}
