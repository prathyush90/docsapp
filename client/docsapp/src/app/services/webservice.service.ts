import { Injectable } from '@angular/core';
import { Headers, Http,RequestOptions,Response} from '@angular/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/timeout'

@Injectable()
export class WebserviceService {
private headers = new Headers({'Content-Type': 'application/json'});

  constructor(private http:Http) { }


  getResponse(url,obj){
  	
  		return this.http
	    .post(url, JSON.stringify(obj), {headers: this.headers})
	    .toPromise()
	    
  	
  }

}
