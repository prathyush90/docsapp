import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { RouterModule }   from '@angular/router';
import { AppComponent } from './app.component';
import { DriversComponent } from './components/drivers/drivers.component';
import { UsersComponent } from './components/users/users.component';
import { AdminComponent } from './components/admin/admin.component';
import { WebserviceService } from './services/webservice.service';
import { SocketService } from './services/socket.service';
import { DialogService } from './services/dialog.service';
import { ErrorpageComponent } from './components/errorpage/errorpage.component';
import { DialogComponent } from './components/dialog/dialog.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';

@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    DriversComponent,
    UsersComponent,
    ErrorpageComponent,
    DialogComponent,
    DashboardComponent,
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    NgbModule.forRoot(),
    RouterModule.forRoot([

      {
        path: 'admin',
        component:AdminComponent
      },
      {
        path: 'dashboard',
        component:DashboardComponent
      },

      {
        path: 'drivers/:id',
        component:DriversComponent
      },

      {
        path: 'users',
        component:UsersComponent
      },

      {
        path: 'error',
        component:ErrorpageComponent
      },

      {
        path: '',
        redirectTo: '/dashboard',
        pathMatch: 'full'
      }
      
    ]),
    HttpModule
  ],
  providers: [SocketService,WebserviceService,DialogService],
  entryComponents:[DialogComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
