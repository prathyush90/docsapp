import { Component, OnInit } from '@angular/core';
import { WebserviceService } from '../../services/webservice.service'
@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  driverid:string;
  driverx:number;
  drivery:number;

  constructor(private httpService:WebserviceService) { }

  ngOnInit() {
  }

  addDriver(){
  	if(this.driverid)
  	{
  		this.httpService.getResponse('/adminapi/adddriver',{driver_id:this.driverid,driver_x:this.driverx,driver_y:this.drivery})
  		.then(resp=>{
  			let data = resp.json();
  			if(!data['success']){
  				console.log(data['err']);
  			}else{
  				this.driverid = '';
  				this.driverx  = 0;
  				this.drivery = 0;
  			}
  		})
  		.catch(err=>{
  			console.log(err);
  		})
  	}	

  }

}
