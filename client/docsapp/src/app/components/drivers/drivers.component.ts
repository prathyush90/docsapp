import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params,Router }   from '@angular/router';
import { WebserviceService } from '../../services/webservice.service'
import { SocketService } from '../../services/socket.service'
import { DialogService } from '../../services/dialog.service'
@Component({
  selector: 'app-drivers',
  templateUrl: './drivers.component.html',
  styleUrls: ['./drivers.component.css']
})
export class DriversComponent implements OnInit {

  tab:number=0;	
  driver_id:string;
  data:any[];
  connection:any;
  constructor(private dialogService: DialogService, private chatService:SocketService, private route: ActivatedRoute,private router:Router,private httpService:WebserviceService) { }


  ngOnInit() {

  	this.route.params
  	.subscribe((params) => {
      this.driver_id = params['id'];
      if(this.driver_id){
      	this.callApi('/driverapi/getpending');
      }else{
      	this.router.navigate(['/error']);
			
      }
  	  
    })


    this.connection = this.chatService.getMessages().subscribe((res:any) => {
      //this.messages.push(message);
      try{
        console.log(res);
        console.log(this.data);
        this.handleResponse(res.message,this.data);
      }catch(e){
        console.log(e);
      }
      
      
      
    });
  	
  }


  

  

  


  handleResponse(id,data){
    
    if(data){
      data.forEach(function(request){
      	if(request._id == id){
          
          let index = data.indexOf(request);
          console.log(index);
      		data.splice(index,1);
      	}
      });
    }
  }




  callApi(url){
  	this.httpService.getResponse(url,{driver_id:this.driver_id})	
      .then(resp=>{
      	let dat = resp.json();
      	this.data = dat['data'];
      })
      .catch(err=>{
      	console.log(err);
      });
  }

  tabChange(number){
  	this.tab = number;
  	if(number == 0){
  		this.callApi('/driverapi/getpending')
  	}else if(number == 1){
  		this.callApi('/driverapi/getongoing')
  	}else{
  		this.callApi('/driverapi/getcompleted')
  	}
  }

  getTime(milliseconds){
  	//console.log(milliseconds);
  	var time = new Date();
  	var presenttime = time.getTime();
  	let difference = Math.abs(presenttime - milliseconds)/1000;
  	if(difference < 1){
  		return "just now";
  	}else if(difference < 60){
  		return "less than a minute ago";
  	}else if(difference < 180){
  		return "less than 3 minutes ago";
  	}else if(difference < 300){
  		return "less than 5 minutes ago";
  	}else if(difference < 3600){
  		return "less than an hour ago";
  	}else if(difference < 3600 * 24){
  		return "less than a day ago";
  	}

  	let date = new Date(milliseconds);
  	return this.prettyDate(date);
  }

  prettyDate(date) {
  	var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

  	return months[date.getUTCMonth()] + ' ' + date.getUTCDate() + ', ' + date.getUTCFullYear();
}

requestSelected(request){
  this.checkAllow(request,this.driver_id)
}

processRequest(request){
  this.httpService.getResponse('/requestapi/processrequest',{request:request,driver_id:this.driver_id})
      .then(res=>{
        let data = res.json();
        if(data['success']){
          this.dialogService.openDialog('Succesfully selected.Order will be completed in 5 minutes automatically')
          this.tabChange(1);

        }else{
          this.dialogService.openDialog(data['message'])
          
        }
        console.log(data);
      })
      .catch(err=>{
        this.dialogService.openDialog('Server Error')
      });
}

checkAllow(request,driver_id){
  this.httpService.getResponse('/requestapi/ispossible',{request:request,driver_id:driver_id})
  .then(resp=>{
    let data = resp.json();
    
    if(data['success']){
      //this.tabChange(1);
      this.processRequest(request);
    }else{
      this.dialogService.openDialog(data['message'])
      
    }

  })
  .catch(err=>{
      this.dialogService.openDialog('Server Error')
  })
}

}
