import { Component, OnInit } from '@angular/core';
import { WebserviceService } from '../../services/webservice.service'
import { DialogService } from '../../services/dialog.service'
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
	userid:string;
	userx:number;
	usery:number;
  constructor(private dialogService:DialogService,private httpService:WebserviceService) { }

  ngOnInit() {

  }

  ride1(){
    if(this.userid)
  	{
      this.httpService.getResponse('/requestapi/addrequestv1',{user_id:this.userid})
  		.then(resp=>{
  			let data = resp.json();
  			if(!data['success']){
  				console.log(data['err']);
  			}else{
  				this.userid = '';
  				this.userx  = 0;
  				this.usery = 0;
          this.dialogService.openDialog("Request Placed succesfully!!")
  			}
  		})
  		.catch(err=>{
  			console.log(err);
  		})
  	}else{
      this.dialogService.openDialog("User id is mandatory");
    }
  }

  ride2(){

  	if(this.userid)
  	{
  		this.httpService.getResponse('/requestapi/addrequestv2',{user_id:this.userid,x:this.userx,y:this.usery})
  		.then(resp=>{
  			let data = resp.json();
  			if(!data['success']){
  				console.log(data['err']);
  			}else{
  				this.userid = '';
  				this.userx  = 0;
  				this.usery = 0;
          this.dialogService.openDialog("Request Placed!!")
          
  			}
  		})
  		.catch(err=>{
  			console.log(err);
  		})
  	}else{
      this.dialogService.openDialog("User id is mandatory");
    }
  }

  checkTraffic(number){
    this.httpService.getResponse('/requestapi/checktraffic',{})
      .then(resp=>{
        let data = resp.json();
        console.log(data);
        if(data['success']){
          if(number == 1){
              this.ride1()
            }else{
                this.ride2()
            }
        }else{
          this.dialogService.openDialog("Peak Traffic..Can't accept your order now!!")
          
        }
      })
      .catch(err=>{
        console.log(err);
        this.dialogService.openDialog("Server error")
        
      })
  }

}


