import { Component, OnInit } from '@angular/core';
import { WebserviceService } from '../../services/webservice.service'
import { DialogService } from '../../services/dialog.service'
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
	data:any[];
  constructor(private httpService:WebserviceService,private dialogService:DialogService) { }

  ngOnInit() {
  	this.httpService.getResponse('/requestapi/getdashboard',{})
  	.then(resp=>{
  		let data = resp.json();
  		if(data['success']){
  			this.data = data.data;
  		}else{
  			this.dialogService.openDialog(data.err)
  		}
  	})
  	.catch(err=>{
  		this.dialogService.openDialog("Server error");
  	})
  }



  getStatus(status){
  	if(status == 0){
  		return "Waiting";
  	}else if(status == 1){
  		return "Ongoing"
  	}else{
  		return "Complete"
  	}
  }


  getTime(milliseconds){
  	//console.log(milliseconds);
  	var time = new Date();
  	var presenttime = time.getTime();
  	let difference = Math.abs(presenttime - milliseconds)/1000;
  	if(difference < 1){
  		return "just now";
  	}else if(difference < 60){
  		return "less than a minute ago";
  	}else if(difference < 180){
  		return "less than 3 minutes ago";
  	}else if(difference < 300){
  		return "less than 5 minutes ago";
  	}else if(difference < 3600){
  		return "less than an hour ago";
  	}else if(difference < 3600 * 24){
  		return "less than a day ago";
  	}

  	let date = new Date(milliseconds);
  	return this.prettyDate(date);
  }

  prettyDate(date) {
  	var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

  	return months[date.getUTCMonth()] + ' ' + date.getUTCDate() + ', ' + date.getUTCFullYear();
}


}
