import { Component, OnInit ,Input , ChangeDetectorRef} from '@angular/core';
import {NgbModal, NgbModalOptions, NgbActiveModal,  ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent implements OnInit {
	@Input()
	message:string;
  constructor(public activeModal: NgbActiveModal, public changeRef: ChangeDetectorRef) { }

  ngOnInit() {
  	//console.log(this.message);
  }

  dismiss(){
  	this.activeModal.close(null)
  }

  close(flag){
  	this.activeModal.close(flag)
  }

}
