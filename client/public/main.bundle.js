webpackJsonp([1,5],{

/***/ 141:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__ = __webpack_require__(53);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DialogComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DialogComponent = (function () {
    function DialogComponent(activeModal, changeRef) {
        this.activeModal = activeModal;
        this.changeRef = changeRef;
    }
    DialogComponent.prototype.ngOnInit = function () {
        //console.log(this.message);
    };
    DialogComponent.prototype.dismiss = function () {
        this.activeModal.close(null);
    };
    DialogComponent.prototype.close = function (flag) {
        this.activeModal.close(flag);
    };
    return DialogComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", String)
], DialogComponent.prototype, "message", void 0);
DialogComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Component */])({
        selector: 'app-dialog',
        template: __webpack_require__(289),
        styles: [__webpack_require__(272)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__["c" /* NgbActiveModal */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__["c" /* NgbActiveModal */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* ChangeDetectorRef */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* ChangeDetectorRef */]) === "function" && _b || Object])
], DialogComponent);

var _a, _b;
//# sourceMappingURL=/Users/prathyush/Documents/DocsApp/client/docsapp/src/dialog.component.js.map

/***/ }),

/***/ 142:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_socket_io_client__ = __webpack_require__(335);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_socket_io_client___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_socket_io_client__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SocketService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SocketService = (function () {
    function SocketService() {
    }
    SocketService.prototype.sendEvent = function (event, message) {
        this.socket.emit(event, message);
    };
    SocketService.prototype.getMessages = function () {
        var _this = this;
        var observable = new __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"](function (observer) {
            _this.socket = __WEBPACK_IMPORTED_MODULE_2_socket_io_client__('http://localhost:3500/', { transports: ['websocket'] });
            _this.socket.on('statuschange', function (data) {
                console.log("xxxx");
                observer.next(data);
            });
            return function () {
                _this.socket.disconnect();
            };
        });
        return observable;
    };
    return SocketService;
}());
SocketService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["c" /* Injectable */])(),
    __metadata("design:paramtypes", [])
], SocketService);

//# sourceMappingURL=/Users/prathyush/Documents/DocsApp/client/docsapp/src/socket.service.js.map

/***/ }),

/***/ 181:
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = 181;


/***/ }),

/***/ 182:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__(190);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__(201);




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["a" /* enableProdMode */])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=/Users/prathyush/Documents/DocsApp/client/docsapp/src/main.js.map

/***/ }),

/***/ 194:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = (function () {
    function AppComponent() {
    }
    return AppComponent;
}());
AppComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Component */])({
        selector: 'app-root',
        template: __webpack_require__(286),
        styles: [__webpack_require__(269)]
    })
], AppComponent);

//# sourceMappingURL=/Users/prathyush/Documents/DocsApp/client/docsapp/src/app.component.js.map

/***/ }),

/***/ 195:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ng_bootstrap_ng_bootstrap__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_router__ = __webpack_require__(95);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__(194);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_drivers_drivers_component__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__components_users_users_component__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__components_admin_admin_component__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__services_webservice_service__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__services_socket_service__ = __webpack_require__(142);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__services_dialog_service__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__components_errorpage_errorpage_component__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__components_dialog_dialog_component__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__components_dashboard_dashboard_component__ = __webpack_require__(197);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
















var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["b" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_9__components_admin_admin_component__["a" /* AdminComponent */],
            __WEBPACK_IMPORTED_MODULE_7__components_drivers_drivers_component__["a" /* DriversComponent */],
            __WEBPACK_IMPORTED_MODULE_8__components_users_users_component__["a" /* UsersComponent */],
            __WEBPACK_IMPORTED_MODULE_13__components_errorpage_errorpage_component__["a" /* ErrorpageComponent */],
            __WEBPACK_IMPORTED_MODULE_14__components_dialog_dialog_component__["a" /* DialogComponent */],
            __WEBPACK_IMPORTED_MODULE_15__components_dashboard_dashboard_component__["a" /* DashboardComponent */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_4__ng_bootstrap_ng_bootstrap__["a" /* NgbModule */].forRoot(),
            __WEBPACK_IMPORTED_MODULE_5__angular_router__["a" /* RouterModule */].forRoot([
                {
                    path: 'admin',
                    component: __WEBPACK_IMPORTED_MODULE_9__components_admin_admin_component__["a" /* AdminComponent */]
                },
                {
                    path: 'dashboard',
                    component: __WEBPACK_IMPORTED_MODULE_15__components_dashboard_dashboard_component__["a" /* DashboardComponent */]
                },
                {
                    path: 'drivers/:id',
                    component: __WEBPACK_IMPORTED_MODULE_7__components_drivers_drivers_component__["a" /* DriversComponent */]
                },
                {
                    path: 'users',
                    component: __WEBPACK_IMPORTED_MODULE_8__components_users_users_component__["a" /* UsersComponent */]
                },
                {
                    path: 'error',
                    component: __WEBPACK_IMPORTED_MODULE_13__components_errorpage_errorpage_component__["a" /* ErrorpageComponent */]
                },
                {
                    path: '',
                    redirectTo: '/dashboard',
                    pathMatch: 'full'
                }
            ]),
            __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* HttpModule */]
        ],
        providers: [__WEBPACK_IMPORTED_MODULE_11__services_socket_service__["a" /* SocketService */], __WEBPACK_IMPORTED_MODULE_10__services_webservice_service__["a" /* WebserviceService */], __WEBPACK_IMPORTED_MODULE_12__services_dialog_service__["a" /* DialogService */]],
        entryComponents: [__WEBPACK_IMPORTED_MODULE_14__components_dialog_dialog_component__["a" /* DialogComponent */]],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=/Users/prathyush/Documents/DocsApp/client/docsapp/src/app.module.js.map

/***/ }),

/***/ 196:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_webservice_service__ = __webpack_require__(29);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AdminComponent = (function () {
    function AdminComponent(httpService) {
        this.httpService = httpService;
    }
    AdminComponent.prototype.ngOnInit = function () {
    };
    AdminComponent.prototype.addDriver = function () {
        var _this = this;
        if (this.driverid) {
            this.httpService.getResponse('/adminapi/adddriver', { driver_id: this.driverid, driver_x: this.driverx, driver_y: this.drivery })
                .then(function (resp) {
                var data = resp.json();
                if (!data['success']) {
                    console.log(data['err']);
                }
                else {
                    _this.driverid = '';
                    _this.driverx = 0;
                    _this.drivery = 0;
                }
            })
                .catch(function (err) {
                console.log(err);
            });
        }
    };
    return AdminComponent;
}());
AdminComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Component */])({
        selector: 'app-admin',
        template: __webpack_require__(287),
        styles: [__webpack_require__(270)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_webservice_service__["a" /* WebserviceService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_webservice_service__["a" /* WebserviceService */]) === "function" && _a || Object])
], AdminComponent);

var _a;
//# sourceMappingURL=/Users/prathyush/Documents/DocsApp/client/docsapp/src/admin.component.js.map

/***/ }),

/***/ 197:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_webservice_service__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_dialog_service__ = __webpack_require__(37);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DashboardComponent = (function () {
    function DashboardComponent(httpService, dialogService) {
        this.httpService = httpService;
        this.dialogService = dialogService;
    }
    DashboardComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.httpService.getResponse('/requestapi/getdashboard', {})
            .then(function (resp) {
            var data = resp.json();
            if (data['success']) {
                _this.data = data.data;
            }
            else {
                _this.dialogService.openDialog(data.err);
            }
        })
            .catch(function (err) {
            _this.dialogService.openDialog("Server error");
        });
    };
    DashboardComponent.prototype.getStatus = function (status) {
        if (status == 0) {
            return "Waiting";
        }
        else if (status == 1) {
            return "Ongoing";
        }
        else {
            return "Complete";
        }
    };
    DashboardComponent.prototype.getTime = function (milliseconds) {
        //console.log(milliseconds);
        var time = new Date();
        var presenttime = time.getTime();
        var difference = Math.abs(presenttime - milliseconds) / 1000;
        if (difference < 1) {
            return "just now";
        }
        else if (difference < 60) {
            return "less than a minute ago";
        }
        else if (difference < 180) {
            return "less than 3 minutes ago";
        }
        else if (difference < 300) {
            return "less than 5 minutes ago";
        }
        else if (difference < 3600) {
            return "less than an hour ago";
        }
        else if (difference < 3600 * 24) {
            return "less than a day ago";
        }
        var date = new Date(milliseconds);
        return this.prettyDate(date);
    };
    DashboardComponent.prototype.prettyDate = function (date) {
        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
            'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        return months[date.getUTCMonth()] + ' ' + date.getUTCDate() + ', ' + date.getUTCFullYear();
    };
    return DashboardComponent;
}());
DashboardComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Component */])({
        selector: 'app-dashboard',
        template: __webpack_require__(288),
        styles: [__webpack_require__(271)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_webservice_service__["a" /* WebserviceService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_webservice_service__["a" /* WebserviceService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__services_dialog_service__["a" /* DialogService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_dialog_service__["a" /* DialogService */]) === "function" && _b || Object])
], DashboardComponent);

var _a, _b;
//# sourceMappingURL=/Users/prathyush/Documents/DocsApp/client/docsapp/src/dashboard.component.js.map

/***/ }),

/***/ 198:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(95);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_webservice_service__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_socket_service__ = __webpack_require__(142);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_dialog_service__ = __webpack_require__(37);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DriversComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var DriversComponent = (function () {
    function DriversComponent(dialogService, chatService, route, router, httpService) {
        this.dialogService = dialogService;
        this.chatService = chatService;
        this.route = route;
        this.router = router;
        this.httpService = httpService;
        this.tab = 0;
    }
    DriversComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params
            .subscribe(function (params) {
            _this.driver_id = params['id'];
            if (_this.driver_id) {
                _this.callApi('/driverapi/getpending');
            }
            else {
                _this.router.navigate(['/error']);
            }
        });
        this.connection = this.chatService.getMessages().subscribe(function (res) {
            //this.messages.push(message);
            try {
                console.log(res);
                console.log(_this.data);
                _this.handleResponse(res.message);
            }
            catch (e) {
                console.log(e);
            }
        });
    };
    DriversComponent.prototype.handleResponse = function (id) {
        if (this.data) {
            this.data.forEach(function (request) {
                if (request._id == id) {
                    var index = this.data.indexOf(request);
                    console.log(index);
                    this.data.splice(index, 1);
                }
            });
        }
    };
    DriversComponent.prototype.callApi = function (url) {
        var _this = this;
        this.httpService.getResponse(url, { driver_id: this.driver_id })
            .then(function (resp) {
            var dat = resp.json();
            _this.data = dat['data'];
        })
            .catch(function (err) {
            console.log(err);
        });
    };
    DriversComponent.prototype.tabChange = function (number) {
        this.tab = number;
        if (number == 0) {
            this.callApi('/driverapi/getpending');
        }
        else if (number == 1) {
            this.callApi('/driverapi/getongoing');
        }
        else {
            this.callApi('/driverapi/getcompleted');
        }
    };
    DriversComponent.prototype.getTime = function (milliseconds) {
        //console.log(milliseconds);
        var time = new Date();
        var presenttime = time.getTime();
        var difference = Math.abs(presenttime - milliseconds) / 1000;
        if (difference < 1) {
            return "just now";
        }
        else if (difference < 60) {
            return "less than a minute ago";
        }
        else if (difference < 180) {
            return "less than 3 minutes ago";
        }
        else if (difference < 300) {
            return "less than 5 minutes ago";
        }
        else if (difference < 3600) {
            return "less than an hour ago";
        }
        else if (difference < 3600 * 24) {
            return "less than a day ago";
        }
        var date = new Date(milliseconds);
        return this.prettyDate(date);
    };
    DriversComponent.prototype.prettyDate = function (date) {
        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
            'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        return months[date.getUTCMonth()] + ' ' + date.getUTCDate() + ', ' + date.getUTCFullYear();
    };
    DriversComponent.prototype.requestSelected = function (request) {
        this.checkAllow(request, this.driver_id);
    };
    DriversComponent.prototype.processRequest = function (request) {
        var _this = this;
        this.httpService.getResponse('/requestapi/processrequest', { request: request, driver_id: this.driver_id })
            .then(function (res) {
            var data = res.json();
            if (data['success']) {
                _this.dialogService.openDialog('Succesfully selected.Order will be completed in 5 minutes automatically');
                _this.tabChange(1);
            }
            else {
                _this.dialogService.openDialog(data['message']);
            }
            console.log(data);
        })
            .catch(function (err) {
            _this.dialogService.openDialog('Server Error');
        });
    };
    DriversComponent.prototype.checkAllow = function (request, driver_id) {
        var _this = this;
        this.httpService.getResponse('/requestapi/ispossible', { request: request, driver_id: driver_id })
            .then(function (resp) {
            var data = resp.json();
            if (data['success']) {
                //this.tabChange(1);
                _this.processRequest(request);
            }
            else {
                _this.dialogService.openDialog(data['message']);
            }
        })
            .catch(function (err) {
            _this.dialogService.openDialog('Server Error');
        });
    };
    return DriversComponent;
}());
DriversComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Component */])({
        selector: 'app-drivers',
        template: __webpack_require__(290),
        styles: [__webpack_require__(273)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_4__services_dialog_service__["a" /* DialogService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services_dialog_service__["a" /* DialogService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__services_socket_service__["a" /* SocketService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_socket_service__["a" /* SocketService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_2__services_webservice_service__["a" /* WebserviceService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_webservice_service__["a" /* WebserviceService */]) === "function" && _e || Object])
], DriversComponent);

var _a, _b, _c, _d, _e;
//# sourceMappingURL=/Users/prathyush/Documents/DocsApp/client/docsapp/src/drivers.component.js.map

/***/ }),

/***/ 199:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ErrorpageComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ErrorpageComponent = (function () {
    function ErrorpageComponent() {
    }
    ErrorpageComponent.prototype.ngOnInit = function () {
    };
    return ErrorpageComponent;
}());
ErrorpageComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Component */])({
        selector: 'app-errorpage',
        template: __webpack_require__(291),
        styles: [__webpack_require__(274)]
    }),
    __metadata("design:paramtypes", [])
], ErrorpageComponent);

//# sourceMappingURL=/Users/prathyush/Documents/DocsApp/client/docsapp/src/errorpage.component.js.map

/***/ }),

/***/ 200:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_webservice_service__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_dialog_service__ = __webpack_require__(37);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UsersComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var UsersComponent = (function () {
    function UsersComponent(dialogService, httpService) {
        this.dialogService = dialogService;
        this.httpService = httpService;
    }
    UsersComponent.prototype.ngOnInit = function () {
    };
    UsersComponent.prototype.ride1 = function () {
        var _this = this;
        if (this.userid) {
            this.httpService.getResponse('/requestapi/addrequestv1', { user_id: this.userid })
                .then(function (resp) {
                var data = resp.json();
                if (!data['success']) {
                    console.log(data['err']);
                }
                else {
                    _this.userid = '';
                    _this.userx = 0;
                    _this.usery = 0;
                    _this.dialogService.openDialog("Request Placed succesfully!!");
                }
            })
                .catch(function (err) {
                console.log(err);
            });
        }
        else {
            this.dialogService.openDialog("User id is mandatory");
        }
    };
    UsersComponent.prototype.ride2 = function () {
        var _this = this;
        if (this.userid) {
            this.httpService.getResponse('/requestapi/addrequestv2', { user_id: this.userid, x: this.userx, y: this.usery })
                .then(function (resp) {
                var data = resp.json();
                if (!data['success']) {
                    console.log(data['err']);
                }
                else {
                    _this.userid = '';
                    _this.userx = 0;
                    _this.usery = 0;
                    _this.dialogService.openDialog("Request Placed!!");
                }
            })
                .catch(function (err) {
                console.log(err);
            });
        }
        else {
            this.dialogService.openDialog("User id is mandatory");
        }
    };
    UsersComponent.prototype.checkTraffic = function (number) {
        var _this = this;
        this.httpService.getResponse('/requestapi/checktraffic', {})
            .then(function (resp) {
            var data = resp.json();
            console.log(data);
            if (data['success']) {
                if (number == 1) {
                    _this.ride1();
                }
                else {
                    _this.ride2();
                }
            }
            else {
                _this.dialogService.openDialog("Peak Traffic..Can't accept your order now!!");
            }
        })
            .catch(function (err) {
            console.log(err);
            _this.dialogService.openDialog("Server error");
        });
    };
    return UsersComponent;
}());
UsersComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Component */])({
        selector: 'app-users',
        template: __webpack_require__(292),
        styles: [__webpack_require__(275)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__services_dialog_service__["a" /* DialogService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_dialog_service__["a" /* DialogService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__services_webservice_service__["a" /* WebserviceService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_webservice_service__["a" /* WebserviceService */]) === "function" && _b || Object])
], UsersComponent);

var _a, _b;
//# sourceMappingURL=/Users/prathyush/Documents/DocsApp/client/docsapp/src/users.component.js.map

/***/ }),

/***/ 201:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=/Users/prathyush/Documents/DocsApp/client/docsapp/src/environment.js.map

/***/ }),

/***/ 269:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(11)();
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 270:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(11)();
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 271:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(11)();
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 272:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(11)();
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 273:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(11)();
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 274:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(11)();
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 275:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(11)();
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 286:
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\n"

/***/ }),

/***/ 287:
/***/ (function(module, exports) {

module.exports = "<div class=\"panel panel-default panel-v-full\">\n\t<div class=\"panel-header\">\n\t\tAdd Drivers :\t\n\t</div>\n\t<div class=\"panel-body\">\n\t\t<div class=\"row\">\n\t\t\t<div class=\"form-group\">\n\t\t\t\t<label>\n\t\t\t\t\tDriver Id :\n\t\t\t\t</label>\n\t\t\t\t<input type=\"text\" [(ngModel)]=\"driverid\">\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"row\">\n\t\t\t<div class=\"form-group\">\n\t\t\t\t<label>\n\t\t\t\t\tDriver X :\n\t\t\t\t</label>\n\t\t\t\t<input type=\"text\" [(ngModel)]=\"driverx\">\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"row\">\n\t\t\t<div class=\"form-group\">\n\t\t\t\t<label>\n\t\t\t\t\tDriver Y :\n\t\t\t\t</label>\n\t\t\t\t<input type=\"text\" [(ngModel)]=\"drivery\">\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"row\">\n\t\t\t<button (click)=\"addDriver()\" class=\"btn btn-primary\">\n\t\t\t\tAdd\n\t\t\t</button>\n\t\t</div>\t\n\t</div>\n</div>\t\t\n"

/***/ }),

/***/ 288:
/***/ (function(module, exports) {

module.exports = "<div class=\"panel panel-default panel-v-full\">\n\t\n\t<div class=\"panel-header\">\n\t\tDashboard :\t\n\t</div>\n\n\n\t<div class=\"panel-body\" *ngIf=\"data\">\n\n\n\t\t<table class=\"table bill-tbl\">\n\t\t    <thead>\n\t\t      <tr>\n\t\t        <th>Req id</th>\n\t\t        <th>Cust id</th>\n\t\t        <th>Time</th>\n\t\t        <th>Status</th>\n\t\t        <th>Driver Id</th>\n\t\t      </tr>\n\t\t    </thead>\n\t\t    <tbody>\n\t\t      <tr *ngFor=\"let request of data\">\n\t\t        <td>{{request._id}}</td>\n\t\t        <td>{{request.user_id}}</td>\n\t\t        <td>{{getTime(request.request_start_time)}}</td>\n\t\t        <td>{{getStatus(request.request_status)}}</td>\n\t\t        <td>{{request.driver_id}}</td>\n\t\t      </tr>\n\t\t    </tbody>\n\t\t</table>    \n\t\t<!-- <div class=\"row\" *ngFor=\"let request of data\">\n\t\t\t<p>\n\t\t\t\tReq id : {{request._id}}\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\tCust id : {{request.user_id}}\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t{{getTime(request.request_start_time)}}\n\t\t\t</p>\n\t\t\t\t\n\t\t</div> -->\n\t</div>\t\n</div>"

/***/ }),

/***/ 289:
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-header\">\n      <h4 class=\"modal-title\">Alert</h4>\n      <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"dismiss()\">\n        <span aria-hidden=\"true\">&times;</span>\n      </button>\n</div>\n<div class=\"modal-body\">\n\t{{message}}\n</div>\n\n    <div class=\"modal-footer\">\n      <button type=\"button\" class=\"btn btn-secondary\" (click)=\"close(false)\">Cancel</button>\n      <button type=\"button\" class=\"btn btn-secondary\" (click)=\"close(true)\">Ok</button>\n</div>\n "

/***/ }),

/***/ 29:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise__ = __webpack_require__(300);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch__ = __webpack_require__(298);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_timeout__ = __webpack_require__(299);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_timeout___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_timeout__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WebserviceService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var WebserviceService = (function () {
    function WebserviceService(http) {
        this.http = http;
        this.headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ 'Content-Type': 'application/json' });
    }
    WebserviceService.prototype.getResponse = function (url, obj) {
        return this.http
            .post(url, JSON.stringify(obj), { headers: this.headers })
            .toPromise();
    };
    return WebserviceService;
}());
WebserviceService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["c" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */]) === "function" && _a || Object])
], WebserviceService);

var _a;
//# sourceMappingURL=/Users/prathyush/Documents/DocsApp/client/docsapp/src/webservice.service.js.map

/***/ }),

/***/ 290:
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-default\">\n  <div class=\"container-fluid\">\n    <ul class=\"nav navbar-nav\">\n      <li [ngClass]=\"tab == 0 ? 'active' : '' \"><a (click) = \"tabChange(0)\" >Pending</a></li>\n      <li [ngClass]=\"tab == 1 ? 'active' : '' \"><a (click) = \"tabChange(1)\">OnGoing</a></li>\n      <li [ngClass]=\"tab == 2 ? 'active' : '' \"><a (click) = \"tabChange(2)\">Completed</a></li>\n    </ul>\n  </div>\n</nav>\n\n\n\n\n<div class=\"panel panel-default panel-v-full\">\n\t\n\t<div class=\"panel-body\" *ngIf=\"data\">\n\n\n\t\t<table class=\"table bill-tbl\">\n\t\t    <thead>\n\t\t      <tr>\n\t\t        <th>Req id</th>\n\t\t        <th>Cust id</th>\n\t\t        <th>Time</th>\n\t\t        <th></th>\n\t\t      </tr>\n\t\t    </thead>\n\t\t    <tbody>\n\t\t      <tr *ngFor=\"let request of data\">\n\t\t        <td>{{request._id}}</td>\n\t\t        <td>{{request.user_id}}</td>\n\t\t        <td>{{getTime(request.request_start_time)}}</td>\n\t\t        <td><button (click)=\"requestSelected(request)\" *ngIf=\"request.request_status==0\" class=\"btn btn-primary\">\n\t\t\t\tSelect\n\t\t\t</button></td>\n\t\t      </tr>\n\t\t    </tbody>\n\t\t</table>    \n\t\t<!-- <div class=\"row\" *ngFor=\"let request of data\">\n\t\t\t<p>\n\t\t\t\tReq id : {{request._id}}\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\tCust id : {{request.user_id}}\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\t{{getTime(request.request_start_time)}}\n\t\t\t</p>\n\t\t\t\t\n\t\t</div> -->\n\t</div>\t\n</div>\n\n\n\n"

/***/ }),

/***/ 291:
/***/ (function(module, exports) {

module.exports = "<p>\n  errorpage works!\n</p>\n"

/***/ }),

/***/ 292:
/***/ (function(module, exports) {

module.exports = "<div class=\"panel panel-default panel-v-full\">\n\t<div class=\"panel-header\">\n\t\tRide Now :\t\n\t</div>\n\t<div class=\"panel-body\">\n\t\t<div class=\"row\">\n\t\t\t<div class=\"form-group\">\n\t\t\t\t<label>\n\t\t\t\t\tUser Id :\n\t\t\t\t</label>\n\t\t\t\t<input type=\"text\" [(ngModel)]=\"userid\">\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"row\">\n\t\t\t<div class=\"form-group\">\n\t\t\t\t<label>\n\t\t\t\t\tUser X :\n\t\t\t\t</label>\n\t\t\t\t<input onkeypress=' return event.charCode >= 48 && event.charCode <= 57 ' type=\"text\" [(ngModel)]=\"userx\">\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"row\">\n\t\t\t<div class=\"form-group\">\n\t\t\t\t<label>\n\t\t\t\t\tUser Y :\n\t\t\t\t</label>\n\t\t\t\t<input onkeypress=' return event.charCode >= 48 && event.charCode <= 57 ' type=\"text\" [(ngModel)]=\"usery\">\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"row\">\n\t\t\t<div class=\"col-sm-3 col-offset-sm-3\">\n\t\t\t\t<button (click)=\"checkTraffic(1)\" class=\"btn btn-primary\">\n\t\t\t\t\tRideNow v1\n\t\t\t\t</button>\n\t\t\t</div>\n\t\t\t<div class=\"col-sm-3 col-offset-sm-3\">\n\t\t\t\t<button (click)=\"checkTraffic(2)\" class=\"btn btn-primary\">\n\t\t\t\t\tRideNow v2\n\t\t\t\t</button>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"row\" *ngIf=\"message\">\n\t\t\t<h1>{{message}}</h1>\n\t\t</div>\t\n\t</div>\n</div>\t\t\n"

/***/ }),

/***/ 345:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 346:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(182);


/***/ }),

/***/ 37:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_dialog_dialog_component__ = __webpack_require__(141);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DialogService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DialogService = (function () {
    function DialogService(modalService) {
        this.modalService = modalService;
    }
    DialogService.prototype.openDialog = function (message) {
        console.log(message);
        var modalRef = this.modalService.open(__WEBPACK_IMPORTED_MODULE_2__components_dialog_dialog_component__["a" /* DialogComponent */]);
        modalRef.componentInstance.message = message;
        modalRef.result.then(function (data) {
            console.log(data);
        })
            .catch(function (err) {
            console.log(err);
        });
    };
    return DialogService;
}());
DialogService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["c" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__["b" /* NgbModal */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__["b" /* NgbModal */]) === "function" && _a || Object])
], DialogService);

var _a;
//# sourceMappingURL=/Users/prathyush/Documents/DocsApp/client/docsapp/src/dialog.service.js.map

/***/ })

},[346]);
//# sourceMappingURL=main.bundle.js.map