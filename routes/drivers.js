var express = require('express');
var router = express.Router();
var driverSchema = require('../models/drivers.js')
var requestSchema = require('../models/requests.js')






router.post('/getpending',function(req,res,next){

	var driver_id   = req.body.driver_id;

	requestSchema.forDriver(driver_id,0)
	.then(data=>{
		res.status(200).send({success:true,data:data,status:1})
	})
	.catch(err=>{
		res.status(200).send({success:false,err:err.toString(),status:0})
	})



});


router.post('/getongoing',function(req,res,next){

	var driver_id   = req.body.driver_id;

	requestSchema.forDriverWithStatus(driver_id,1)
	.then(data=>{
		res.status(200).send({success:true,data:data,status:1})
	})
	.catch(err=>{
		res.status(200).send({success:false,err:err.toString(),status:0})
	})



});

router.post('/getcompleted',function(req,res,next){

	var driver_id   = req.body.driver_id;

	requestSchema.forDriverWithStatus(driver_id,2)
	.then(data=>{
		res.status(200).send({success:true,data:data,status:1})
	})
	.catch(err=>{
		res.status(200).send({success:false,err:err.toString(),status:0})
	})



});











module.exports = router;