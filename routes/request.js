module.exports = function(socket){


var express = require('express');
var router = express.Router();
var driverSchema = require('../models/drivers.js');
var requestSchema = require('../models/requests.js');




router.post('/addrequestv1',function(req,res,next){

	var userid = req.body.user_id;

	driverSchema.findAllDrivers()
	.then(data=>{
		var date   = new Date();
		var milliseconds = date.getTime();
		var request = new requestSchema({request_start_time:milliseconds,user_id:userid,todrivers:data})
		
		request.save(function(err,data){
			if(err){
				res.status(200).send({success:false,err:err.toString(),status:0})
			}else{
				res.status(200).send({success:true,data:data,status:1})
		
			}
		});

	});


});

router.post('/addrequestv2',function(req,res,next){

	var userid = req.body.user_id;

	driverSchema.findDrivers(req.body.x,req.body.y)
	.then(data=>{
		console.log(data);
		var date   = new Date();
		var milliseconds = date.getTime();
		var request = new requestSchema({request_start_time:milliseconds,user_id:userid,todrivers:data})
		
		request.save(function(err,data){
			if(err){
				res.status(200).send({success:false,err:err.toString(),status:0})
			}else{
				res.status(200).send({success:true,data:data,status:1})
		
			}
		});

	});


});


router.post('/checktraffic',function(req,res,next){

	requestSchema.findCount()
		.then(data=>{
			console.log(data);
			if(data >= 10){
				res.status(200).send({success:false,message:"Agents are busy.Peak time!!"})
			}else{
				res.status(200).send({success:true})
			}
		})

})

router.post('/ispossible',function(req,res,next){

	var driver_id  = req.body.driver_id;

	driverSchema.findById(driver_id)
	.then(data=>{
		if(data.driver_status != 0 ){
			res.status(200).send({success:false,message:"You are already on a trip"})
		}else{
			res.status(200).send({success:true})
		
	}
	})
	.catch(err=>{
		console.log(err);
		res.status(200).send({success:false,message:"Server eroor"})
	});
});


router.post('/processrequest',function(req,res,next){

	var request = req.body.request;
	var driver_id = req.body.driver_id;

	requestSchema.findById(request._id)
	.then(data=>{
		console.log(data);
		if(data.request_status != 0){
			//order picked by some one else
			res.status(200).send({success:false,status:0,message:"Too late some one else picked the order"})
			
		}else{
			Promise.all([requestSchema.update(request._id,driver_id,1),driverSchema.update(driver_id,1)]).then(([request, driver]) => {
                var resp = {};
                resp.request = request;
                resp.driver  = driver;
                res.status(200).send({success:true,status:1,data:resp})
                socket.emit('statuschange',request._id);
          	}).catch(err=>{
                res.status(200).send({success:false,status:2,message:err.toString()})
          	});
		}
	})
	.catch(err=>{
		res.status(200).send({success:false,status:2,err:err.toString()})
	});

});

router.post('/getdashboard',function(req,res,next){

	requestSchema.findAll()
	.then(data=>{
		res.status(200).send({success:true,status:0,data:data})
	})
	.catch(err=>{
		res.status(200).send({success:false,status:1,err:err.toString()})
	})

});









return router;


}